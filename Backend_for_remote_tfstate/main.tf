# Azure Provider configuration
provider "azurerm" {
  features {}
}

module "storage" {
  source  = "../modules/storage"

  # By default, this module will not create a resource group
  # proivde a name to use an existing resource group, specify the existing resource group name,
  # and set the argument to `create_resource_group = false`. Location will be same as existing RG.
  create_resource_group = true
  resource_group_name   = "Terraform_Backend_Resource_Group"
  location              = "Germany West Central"
  storage_account_name  = "terraformbackendstoracc"
  # To enable advanced threat protection set argument to `true`
  enable_advanced_threat_protection = false

  # Container lists with access_type to create
  containers_list = [
    { name = "vm-master-backend", access_type = "private" }
  ]

  # Adding TAG's to your Azure resources (Required)
  # ProjectName and Env are already declared above, to use them here, create a varible.
  tags = {
    ProjectName  = "terraform-backend"
    Env          = "dev"
    Owner        = "arous"
  }
  remote_backend        = true
  remote_config_path   = "../VM_Master/backend"
}

