# Remote-Backend+VM-Automation

Ce projet consiste à automatiser la création d'un remote backend et d'une VM sur Azure avec Terraform en appliquant les notions de sécurité (ne pas exposer les passwords).

Cette VM va exploiter automatiquement le backend crée, stocker terraform state sur ce backend et éxécuter le script à l'aide de local_exec de Terraform. (voir la figure ci-dessous)

![diagram-vm_master.png](/img/diagram-vm_master.png)
