#!/bin/bash

# pre-requisits for terraform and packer
sudo dnf install -y dnf-utils
sudo dnf config-manager --add-repo https://rpm.releases.hashicorp.com/RHEL/hashicorp.repo

# install terraform
sudo dnf -y install terraform
echo 'complete -C /usr/bin/terraform terraform' >> /home/$(whoami)/.bashrc

# install packer
sudo dnf -y install packer
echo 'complete -C /usr/bin/packer packer' >> /home/$(whoami)/.bashrc

# install azure-cli
sudo rpm --import https://packages.microsoft.com/keys/microsoft.asc
echo -e "[azure-cli]
name=Azure CLI
baseurl=https://packages.microsoft.com/yumrepos/azure-cli
enabled=1
gpgcheck=1
gpgkey=https://packages.microsoft.com/keys/microsoft.asc" | sudo tee /etc/yum.repos.d/azure-cli.repo
sudo dnf -y install azure-cli
echo "source /etc/bash_completion.d/azure-cli" >> /home/$(whoami)/.bashrc
