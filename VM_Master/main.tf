provider "azurerm" {
  features {}
}

terraform {
  backend "azurerm" {
    # Do not forget to add -backend-config=backend flag while executing terraform init"
  }
}


resource "azurerm_resource_group" "vm_master_resources" {
  name     = "VM-Master-Resources"
  location = "Germany West Central"
}

module "vm_master" {
  source               = "../modules/vm"
  resource_group_name  = azurerm_resource_group.vm_master_resources.name
  vm_hostname          = "vm-master"
  vm_os_simple         = "RHEL"
  admin_username       = "exa"
  admin_password       = "ExaTech2021"
  # default vm_size    = Standard_D2s_v3
  public_ip_dns        = "vm-master" // change to a unique name per datacenter region
  vnet_subnet_id       = module.network.vnet_subnets[0]
  storage_account_type = "Standard_LRS"
  remote_port          = "22"
  allocation_method    = "Static"
  azure_username       = var.azure_username # set as an env variable using export TF_VAR
  azure_password       = var.azure_password # set as an env variable using export TF_VAR
  depends_on           = [azurerm_resource_group.vm_master_resources]
}

module "network" {
  source              = "../modules/network"
  vnet_name           = "toggl-assignment-network"
  resource_group_name = azurerm_resource_group.vm_master_resources.name
  subnet_prefixes     = ["10.0.1.0/24"]
  subnet_names        = ["subnet1"]

  depends_on = [azurerm_resource_group.vm_master_resources]
}

output "vm_master_public_name" {
  value = module.vm_master.public_ip_dns_name
}

output "vm_master_public_ip" {
  value = module.vm_master.public_ip_address
}
