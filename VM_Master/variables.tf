variable "azure_username" {
    sensitive   = true
}

variable "azure_password" {
    sensitive   = true
}