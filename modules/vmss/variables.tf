variable "name" {
    description = "name of the load balancer"
    default     = "vmss"
}

variable "resource_group" {
    description = "reference to the resource group"
}

variable "health_probe_id" {
    description = "reference health probe"
}

variable "public_ssh_key_data" {
    description = "Public SSH key data"
}
