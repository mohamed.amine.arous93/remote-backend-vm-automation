variable "name" {
    description = "name of the load balancer"
    default     = "load-balancer"
}

variable "resource_group" {
    description = "reference to the resource group"
}

variable "public_ip_address_id" {
    description = "Azure public ip address id"
}

