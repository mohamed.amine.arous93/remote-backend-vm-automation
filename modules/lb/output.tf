output "load_balancer_private_ip_address" {
  value = azurerm_lb.main.frontend_ip_configuration[0].private_ip_address
}