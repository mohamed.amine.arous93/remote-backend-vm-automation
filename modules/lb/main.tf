

resource "azurerm_lb" "main" {
  name                = var.name
  location            = var.resource_group.location
  resource_group_name = var.resource_group.name

  frontend_ip_configuration {
    name                 = "PublicIPAddress"
    public_ip_address_id = public_ip_address_id.id
  }
}

resource "azurerm_lb_backend_address_pool" "bpepool" {
  resource_group_name = var.resource_group.name
  loadbalancer_id     = azurerm_lb.main.id
  name                = "${var.name}-bpepool"
}

resource "azurerm_lb_nat_pool" "lbnatpool" {
  resource_group_name            = var.resource_group.name
  name                           = "${var.name}-lbnatpool-ssh"
  loadbalancer_id                = azurerm_lb.main.id
  protocol                       = "Tcp"
  frontend_port_start            = 50000
  frontend_port_end              = 50119
  backend_port                   = 22
  frontend_ip_configuration_name = "PublicIPAddress"
}


resource "azurerm_lb_probe" "main" {
  resource_group_name = var.resource_group.name
  loadbalancer_id     = azurerm_lb.main.id
  name                = "http-probe"
  protocol            = "Http"
  request_path        = "/"
  port                = 80
}


